﻿using System.Collections.Generic;

namespace Cabify.Promotion.Domain.Interfaces
{
    public interface IPromotionRepository
    {
        IEnumerable<IPromotion> GetAll();
        bool Create(IPromotion promotion);
    }
}
