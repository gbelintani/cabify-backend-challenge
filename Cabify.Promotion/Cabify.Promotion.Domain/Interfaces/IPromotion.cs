﻿using Cabify.Promotion.Domain.Models;
using System.Collections.Generic;

namespace Cabify.Promotion.Domain.Interfaces
{
    /// <summary>
    /// With this interface we can create promotions similar to the existing ones with some ease.
    /// Each class that implements this interface needs to implement the business logic regarding the promotion it represents.
    /// </summary>
    public interface IPromotion
    {
        string Code { get; }
        Product EligibleProduct { get; set; }
        bool IsActive { get; }

        bool IsEligible(int qtyEligibleProductsInBasket);
        IEnumerable<Product> ApplyPromotion(int matchingProducts);


    }
}
