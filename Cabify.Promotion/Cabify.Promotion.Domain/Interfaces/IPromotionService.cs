﻿using Cabify.Promotion.Domain.Models;
using System.Collections.Generic;

namespace Cabify.Promotion.Domain.Interfaces
{
    public interface IPromotionService
    {
        IEnumerable<Product> ApplyPromotions(IEnumerable<Product> basketProducts);
    }
}
