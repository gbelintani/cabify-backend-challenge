﻿using Cabify.Promotion.Domain.Interfaces;
using Cabify.Promotion.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cabify.Promotion.Domain.Services
{
    public class PromotionService : IPromotionService
    {
        private readonly IPromotionRepository _promotionRepository;

        public PromotionService(IPromotionRepository promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }

        /// <summary>
        /// Since we only need to know the products in the basket, we don't need the Basket entity, 
        /// then we have one less coupled entity in this service
        /// </summary>
        /// <param name="basketProducts"></param>
        /// <returns></returns>
        public IEnumerable<Product> ApplyPromotions(IEnumerable<Product> basketProducts)
        {
            var promotionProducts = new List<Product>();
            var promotions = GetActivePromotions();
            foreach (var promotion in promotions)
            {
                var matchingProducts = GetMatchingProductsQuantity(basketProducts, promotion.EligibleProduct);
                if (promotion.IsEligible(matchingProducts))
                {
                    promotion.EligibleProduct = basketProducts.FirstOrDefault(x => x.Code.Equals(promotion.EligibleProduct.Code));
                    var promotionItems = promotion.ApplyPromotion(matchingProducts);
                    promotionProducts = promotionProducts.Concat(promotionItems).ToList();
                }
            }
            return promotionProducts;
        }

        private IEnumerable<IPromotion> GetActivePromotions()
        {
            var promotions = _promotionRepository.GetAll();
            if (promotions == null)
            {
                return new List<IPromotion>();
            }
            return promotions.Where(x=>x.IsActive);
        }

        private int GetMatchingProductsQuantity(IEnumerable<Product> basketProducts, Product eligibleProduct)
        {
            return basketProducts.Count(x => x.Code.Equals(eligibleProduct.Code));
        }
    }
}
