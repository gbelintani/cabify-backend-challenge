﻿using Cabify.Promotion.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cabify.Promotion.Domain.Models
{
    public class BuyXGetDiscountPromotion : IPromotion
    {
        public int BuyX { get; set; }
        public decimal DiscountedPricePerItem { get; set; }
        public string Code { get; private set; }

        public Product EligibleProduct { get; set; }

        public bool IsActive { get; private set; }

        public BuyXGetDiscountPromotion(string code, Product eligibleProduct, bool isActive, int minimumQuantity, decimal discountedPrice)
        {
            Code = code;
            EligibleProduct = eligibleProduct;
            IsActive = isActive;
            BuyX = minimumQuantity;
            DiscountedPricePerItem = discountedPrice;
        }

        public IEnumerable<Product> ApplyPromotion(int matchingProducts)
        {
            var totalDiscount = matchingProducts * (EligibleProduct.Price - DiscountedPricePerItem);
            var productsPromotion = new List<Product>()
            {
                new Product("PROMOTION_DISCOUNT","Promotion Discount",- totalDiscount)
            };

            return productsPromotion;
        }

        public bool IsEligible(int qtyEligibleProductsInBasket)
        {
            return qtyEligibleProductsInBasket >= BuyX;
        }


    }
}
