﻿using Cabify.Promotion.Domain.Interfaces;
using System.Collections.Generic;

namespace Cabify.Promotion.Domain.Models
{
    public class BuyXGet1FreePromotion : IPromotion
    {
        public int BuyX { get; private set; }

        public string Code { get; private set; }

        public Product EligibleProduct { get; set; }

        public bool IsActive { get; private set; }

        public BuyXGet1FreePromotion(string code, Product eligibleProduct, bool isActive, int minimumQuantity)
        {
            Code = code;
            EligibleProduct = eligibleProduct;
            IsActive = isActive;
            BuyX = minimumQuantity;
        }

        public IEnumerable<Product> ApplyPromotion(int matchingProducts)
        {
            var timesToBeApplied = matchingProducts / BuyX;
            var productsPromotion = new List<Product>();
            for (var i = 0; i < timesToBeApplied; i++)
            {
                productsPromotion.Add(new Product("PROMOTION_DISCOUNT", "Promotion Discount", -EligibleProduct.Price));
            }
            return productsPromotion;
        }

        public bool IsEligible(int qtyEligibleProductsInBasket)
        {
            return qtyEligibleProductsInBasket >= BuyX;
        }

    }
}
