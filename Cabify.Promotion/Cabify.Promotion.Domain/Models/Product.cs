﻿namespace Cabify.Promotion.Domain.Models
{
    /// <summary>
    /// This shared model needs to take place here since the product structure is needed to calculate the promotions
    /// </summary>
    public class Product
    {
        public string Code { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }

        public Product(string code, string name, decimal price)
        {
            Code = code;
            Name = name;
            Price = price;
        }
    }
}
