﻿using Cabify.Promotion.Domain.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cabify.Promotion.Api.Requests
{
    public class ApplyPromotionRequest
    {
        [JsonProperty(PropertyName = "productsInBasket")]
        public IEnumerable<Product> ProductsInBasket { get; set; }
    }
}
