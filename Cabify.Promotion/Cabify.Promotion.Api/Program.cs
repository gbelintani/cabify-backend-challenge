﻿using Cabify.Promotion.Domain.Interfaces;
using Cabify.Promotion.Infrastructure.Repositories;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Cabify.Promotion.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                SetupSeed(services);

            }
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args).UseStartup<Startup>();
        }

        private static void SetupSeed(IServiceProvider serviceProvider)
        {
            new RepositoryInitializer(serviceProvider.GetRequiredService<IPromotionRepository>()).Seed();
        }
    }
}
