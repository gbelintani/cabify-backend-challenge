﻿using Cabify.Promotion.Api.Requests;
using Cabify.Promotion.Api.Response;
using Cabify.Promotion.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;

namespace Cabify.Promotion.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromotionController : ControllerBase
    {
        private readonly IPromotionService _promotionService;
        private readonly ILogger _logger;

        public PromotionController(IPromotionService promotionService, ILogger<PromotionController> logger)
        {
            _promotionService = promotionService;
            _logger = logger;
        }

        [HttpPost]
        public ActionResult<ApplyPromotionResponse> Post([FromBody] ApplyPromotionRequest request)
        {
            try
            {
                if(request.ProductsInBasket == null)
                {
                    return BadRequest();
                }
                return Ok(new ApplyPromotionResponse() { PromotionProducts = _promotionService.ApplyPromotions(request.ProductsInBasket) });

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error applying promotion");
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

    }
}
