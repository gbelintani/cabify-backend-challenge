﻿using Cabify.Promotion.Domain.Models;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cabify.Promotion.Api.Response
{
    public class ApplyPromotionResponse
    {
        [JsonProperty(PropertyName = "promotionProducts")]
        public IEnumerable<Product> PromotionProducts { get; set; }
    }
}
