﻿using Cabify.Promotion.Domain.Interfaces;
using Cabify.Promotion.Domain.Services;
using Cabify.Promotion.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Cabify.Promotion.IoC
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Repositories are singleton since the database is in memory
            services.AddSingleton<IPromotionRepository, PromotionRepository>();

            //Services and applications are scoped so we use the same object per request 
            services.AddScoped<IPromotionService, PromotionService>();
        }
    }
}
