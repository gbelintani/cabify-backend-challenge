﻿using Cabify.Promotion.Domain.Models;
using System.Linq;
using Xunit;

namespace Cabify.Promotion.Test
{
    public class BuyXGetDiscountPromotionTests
    {


        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void IsEligible_QuantityInBasketGreaterOrEqualX_ShouldReturnTrue(int quantity)
        {
            var promotion = new BuyXGetDiscountPromotion("Promo", new Product("P1", "Test", 10), true, 2, 9);

            Assert.True(promotion.IsEligible(quantity));
        }

        [Fact]
        public void IsEligible_QuantityInBasketLesserThenX_ShouldReturnFalse()
        {
            var promotion = new BuyXGetDiscountPromotion("Promo", new Product("P1", "Test", 10), true, 2, 9);
            Assert.False(promotion.IsEligible(1));
        }

        [Fact]
        public void ApplyPromotion_QuantityInBasketGreaterThenX_ShouldReturnPromotionPrice10()
        {
            var promotion = new BuyXGetDiscountPromotion("Promo", new Product("P1", "Test", 10), true, 2, 9);
            var products = promotion.ApplyPromotion(2);

            Assert.NotNull(products);
            Assert.Single(products);
            Assert.Equal(-2, products.First().Price);
        }
    }
}
