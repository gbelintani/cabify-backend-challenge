using Cabify.Promotion.Domain.Models;
using Xunit;

namespace Cabify.Promotion.Test
{
    public class ProductTests
    {
        public void ShouldCreateProductWithValues()
        {
            var product = new Product("Code", "Name", 10);
            Assert.Equal("Code", product.Code);
            Assert.Equal("Name", product.Name);
            Assert.Equal(10, product.Price);
        }
    }
}
