﻿using Cabify.Promotion.Domain.Models;
using System.Linq;
using Xunit;

namespace Cabify.Promotion.Test
{
    public class BuyXGet1FreePromotionTests
    {


        [Theory]
        [InlineData(2)]
        [InlineData(3)]
        public void IsEligible_QuantityInBasketGreaterOrEqualX_ShouldReturnTrue(int quantity)
        {
            var promotion = new BuyXGet1FreePromotion("Promo", new Product("P1", "Test", 10), true, 2);

            Assert.True(promotion.IsEligible(quantity));
        }

        [Fact]
        public void IsEligible_QuantityInBasketLesserThenX_ShouldReturnFalse()
        {
            var promotion = new BuyXGet1FreePromotion("Promo", new Product("P1", "Test", 10), true, 2);

            Assert.False(promotion.IsEligible(1));
        }

        [Fact]
        public void ApplyPromotion_QuantityInBasketGreaterThenX_ShouldReturnPromotionPrice10()
        {
            var promotion = new BuyXGet1FreePromotion("Promo", new Product("P1", "Test", 10), true, 2);
            var products = promotion.ApplyPromotion(2);

            Assert.NotNull(products);
            Assert.Single(products);
            Assert.Equal(-10, products.First().Price);
        }

        [Fact]
        public void ApplyPromotion_QuantityInBasketTwiceThenX_ShouldReturnPromotionPrice10()
        {
            var promotion = new BuyXGet1FreePromotion("Promo", new Product("P1", "Test", 10), true, 2);
            var products = promotion.ApplyPromotion(4);

            Assert.NotNull(products);
            Assert.Equal(2, products.Count());
            Assert.Equal(-20, products.Sum(x=>x.Price));
        }
    }
}
