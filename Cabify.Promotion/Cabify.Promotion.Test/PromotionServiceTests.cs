﻿using Cabify.Promotion.Domain.Interfaces;
using Cabify.Promotion.Domain.Models;
using Cabify.Promotion.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Cabify.Promotion.Test
{
    public class PromotionServiceTests : IDisposable
    {
        private Mock<IPromotionRepository> _promotionRepository;

        public PromotionServiceTests()
        {
            _promotionRepository = new Mock<IPromotionRepository>();
        }

        public void Dispose()
        {
            _promotionRepository.VerifyAll();
        }

        [Fact]
        public void ApplyPromotions_ExistsPromotion_ShouldReturn1PromotionItem()
        {
            _promotionRepository.Setup(x => x.GetAll()).Returns(new List<IPromotion>()
            {
                new BuyXGet1FreePromotion("Promo", new Product("P1","test",10), true, 2),
                new BuyXGet1FreePromotion("Promo2", new Product("P1","test",10), false, 2)
            });
            var service = new PromotionService(_promotionRepository.Object);
            var promotionItems = service.ApplyPromotions(new List<Product>(){
                new Product("P1","test",10),new Product("P1","test",10)
            });

            Assert.Single(promotionItems);
            Assert.Equal("PROMOTION_DISCOUNT", promotionItems.First().Code);
            Assert.Equal(-10, promotionItems.First().Price);
        }

        [Fact]
        public void ApplyPromotions_DoesntExistsPromotion_ShouldReturn0PromotionItem()
        {
            var service = new PromotionService(_promotionRepository.Object);
            var promotionItems = service.ApplyPromotions(new List<Product>(){
                new Product("P1","test",10),new Product("P1","test",10)
            });

            Assert.Empty(promotionItems);
        }

        [Fact]
        public void ApplyPromotions_ExistsPromotionButNotEnoughProducts_ShouldReturn0PromotionItem()
        {
            _promotionRepository.Setup(x => x.GetAll()).Returns(new List<IPromotion>()
            {
                new BuyXGet1FreePromotion("Promo", new Product("P1","test",10), true, 2),
                new BuyXGet1FreePromotion("Promo2", new Product("P1","test",10), false, 2)
            });
            var service = new PromotionService(_promotionRepository.Object);
            var promotionItems = service.ApplyPromotions(new List<Product>(){
                new Product("P1","test",10)
            });

            Assert.Empty(promotionItems);
        }

        [Fact]
        public void ApplyPromotions_ExistsPromotionProductInBasketWithDifferentPrice_ShouldReturn1PromotionItemWithBasketProductPrice()
        {
            _promotionRepository.Setup(x => x.GetAll()).Returns(new List<IPromotion>()
            {
                new BuyXGet1FreePromotion("Promo", new Product("P1","test",10), true, 2),
                new BuyXGet1FreePromotion("Promo2", new Product("P1","test",10), false, 2)
            });
            var service = new PromotionService(_promotionRepository.Object);
            var promotionItems = service.ApplyPromotions(new List<Product>(){
                new Product("P1","test",9),new Product("P1","test",9)
            });

            Assert.Single(promotionItems);
            Assert.Equal("PROMOTION_DISCOUNT", promotionItems.First().Code);
            Assert.Equal(-9, promotionItems.First().Price);
        }
    }
}
