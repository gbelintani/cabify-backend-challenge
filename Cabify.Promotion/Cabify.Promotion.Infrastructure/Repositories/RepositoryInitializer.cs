﻿using Cabify.Promotion.Domain.Interfaces;
using Cabify.Promotion.Domain.Models;

namespace Cabify.Promotion.Infrastructure.Repositories
{
    public class RepositoryInitializer
    {
        private readonly IPromotionRepository _promotionRepository;

        /// <summary>
        /// This is just an auxiliary class to seed the proposed promotions
        /// </summary>
        /// <param name="promotionRepository"></param>
        public RepositoryInitializer(IPromotionRepository promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }

        public void Seed()
        {
            _promotionRepository.Create(new BuyXGet1FreePromotion("MARKETING_PROMOTION", new Product("VOUCHER", "Cabify Voucher", 5), true, 2));
            _promotionRepository.Create(new BuyXGetDiscountPromotion("CFO_PROMOTION", new Product("TSHIRT", "Cabify T-Shirt", 20), true, 3, 19));
        }
    }
}
