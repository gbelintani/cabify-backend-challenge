﻿using Cabify.Promotion.Domain.Interfaces;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Cabify.Promotion.Infrastructure.Repositories
{
    /// <summary>
    /// If we had more repositories to worry about, this could easily be transformed in a generic abstract class with <TKey,TData></TKey>
    /// </summary>
    public class PromotionRepository : IPromotionRepository
    {
        private readonly ConcurrentDictionary<string, IPromotion> _keyValues;

        public PromotionRepository()
        {
            _keyValues = new ConcurrentDictionary<string, IPromotion>();
        }

        public bool Create(IPromotion promotion)
        {
            return _keyValues.TryAdd(promotion.Code, promotion);
        }

        public IPromotion Get(string code)
        {
            _keyValues.TryGetValue(code, out var value);
            return value;
        }

        public IEnumerable<IPromotion> GetAll()
        {
            return _keyValues.Values;
        }

    }
}
