# Cabify Backend Challenge
## Guilherme Augusto Belintani
### **Proposed Challenge:**
https://github.com/cabify/backend-challenge

### **Stack used**
- Dotnet Core 2.2
- Docker
- REST

### **How to Run**
You will need docker and docker-compose installed.   
This project has a docker-composer.yml file which can be used to run the project. In the same folder as the yml file run:  
      
    docker-compose up 

This should fire up three applications and the port 51089 will the opened in your localhost. There's no interface in this project, so I provided a Postman collection in the same folder **Cabify.postman_collection.json**. But fell free to use any other applications you prefer.

#### **Endpoints**
- POST - Create a new checkout basket  
    
      http://localhost:51089/api/basket
    Returns: 200 OK - Guid which is used in the next endpoints, 500 - InternalServerError
- PUT - Add a product to a basket  

      http://localhost:51089/api/basket/{Guid}
      Body: {
	        "productCode" : "VOUCHER"
            }
    Takes the Guid generated and a body with the product code.  
    Returns: 200 OK, 404 NotFound, 500 InternalServerError
- GET - Get the total amount in a basket  
     
      http://localhost:51089/api/basket/{Guid}
      Takes the Guid generated
    Returns: 200 Ok - List of products and Total Sum with promotions applied, 404 NotFound, 500 InternalServerError
- DELETE - Remove the basket  
  
        http://localhost:51089/api/basket/{Guid}
        Takes the Guid generated
    Returns: 200 OK, 500 InternalServerError

Products Reference table:
```
Code         | Name                |  Price
-------------------------------------------------
VOUCHER      | Cabify Voucher      |   5.00€
TSHIRT       | Cabify T-Shirt      |  20.00€
MUG          | Cabify Coffee Mug   |   7.50€
```
Promotion Reference rules:
 * The marketing department thinks a buy 2 get 1 free promotion will work best (buy two of the same product, get one free), and would like this to only apply to `VOUCHER` items.

 * The CFO insists that the best way to increase sales is with discounts on bulk purchases (buying x or more of a product, the price of that product is reduced), and requests that if you buy 3 or more `TSHIRT` items, the price per unit should be 19.00€.



### **Architecture**
This is a microservice architecture, although the actual requirements are small and relatively simple, this can take a much larger scale in the future. 
- Product Service: This service is only responsible to store and provide the products we have in our shop.
- Promotion Service: This service is responsible to store and implement the business logic of our promotions.
- Basket Service: This service aggregates it all, all the business logic and orchestration between these projects are made by this service, it will check the product service to be sure to get the most recent information about the products and also make sure the promotions are applied to every basket.

All the projects implement a DDD approach to the project structure, like below:  
- API layer: This will have all the API related configurations and the proper endpoints.
- Application layer: This will be necessary only to the Basket Service, since it has to coordinate information coming from different domain sources
- Domain layer: This layer have the domain core, Models, Interfaces, Services and ACLs
- Infrastructure layer: Here we will code everything that can be put in an external dependency, like databases, message queues, HTTP call implementations and so on.
- IoC layer: This is not a layer per say, but more like a auxiliary project that will handle the dependency injection configuration. Since it has reference to almost all projects, this is necessary to avoid circular dependency.
- Test project: All projects will have the domain and applicaion layers tested to ensure quality.


### **What was not used (But would be good)**
- Cache: Both client and server side caches can be a good choice in cases of high volume. This should be implemented with care, since depending on the data we have the cache strategy has to be well thought.
- gRPC: Implementing gRPC, specially between the services, can result in a much lightweight, therefore faster communication between services.  