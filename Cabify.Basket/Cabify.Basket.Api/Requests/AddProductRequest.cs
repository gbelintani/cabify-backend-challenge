﻿using Newtonsoft.Json;

namespace Cabify.Basket.Api.Requests
{
    public class AddProductRequest
    {
        [JsonProperty(PropertyName = "productCode")]
        public string ProductCode { get; set; }
    }
}
