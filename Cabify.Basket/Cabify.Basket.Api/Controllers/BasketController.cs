﻿using Cabify.Basket.Api.Requests;
using Cabify.Basket.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Cabify.Basket.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly IBasketService _basketService;
        private readonly IManageBasketApplication _manageBasketApplication;
        private readonly ILogger<BasketController> _logger;

        public BasketController(IBasketService basketService, IManageBasketApplication manageBasketApplication, ILogger<BasketController> logger)
        {
            _basketService = basketService;
            _manageBasketApplication = manageBasketApplication;
            _logger = logger;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Domain.Models.Basket>> Get(Guid id)
        {
            try
            {
                if(id == null || id.Equals(Guid.Empty)) { return BadRequest(); }
                var basket = await _manageBasketApplication.GetTotal(id);
                if(basket == null)
                {
                    return NotFound();
                }
                return Ok(basket);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Basket Get Error");
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // POST api/values
        [HttpPost]
        public ActionResult<Guid> Post()
        {
            try
            {
                var basket = _basketService.New();
                return Ok(basket);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Basket Post Error");
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(Guid id, [FromBody] AddProductRequest request)
        {
            try
            {
                if (id == null || id.Equals(Guid.Empty) || request == null ||string.IsNullOrEmpty(request.ProductCode)) { return BadRequest(); }
                var basket = await _manageBasketApplication.AddProduct(id, request.ProductCode);
                if (basket)
                {
                    return Ok();
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Basket Put Error");
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            try
            {
                var basket = _basketService.Remove(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Basket Delete Error");
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
