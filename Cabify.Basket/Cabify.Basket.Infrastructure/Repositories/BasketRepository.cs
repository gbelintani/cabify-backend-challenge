﻿using Cabify.Basket.Domain.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Cabify.Basket.Infrastructure.Repositories
{
    public class BasketRepository : IBasketRepository
    {
        private readonly ConcurrentDictionary<Guid, Domain.Models.Basket> _keyValues;

        public BasketRepository()
        {
            _keyValues = new ConcurrentDictionary<Guid, Domain.Models.Basket>();
        }

        public bool Create(Domain.Models.Basket basket)
        {
            return _keyValues.TryAdd(basket.Id, basket);
        }

        public Domain.Models.Basket Get(Guid id)
        {
            _keyValues.TryGetValue(id, out var value);
            return value;
        }

        public IEnumerable<Domain.Models.Basket> GetAll()
        {
            return _keyValues.Values;
        }

        public bool Remove(Guid basketId)
        {
            return _keyValues.TryRemove(basketId, out var value);
        }

        public bool Update(Domain.Models.Basket basket)
        {
            return _keyValues.TryUpdate(basket.Id, basket, basket);
        }
    }
}
