﻿using Cabify.Basket.Application;
using Cabify.Basket.Domain.ACLs;
using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Services;
using Cabify.Basket.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Cabify.Basket.IoC
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddHttpClient<IProductACL, ProductACL>();
            services.AddHttpClient<IPromotionACL, PromotionACL>();
            //Repositories are singleton since the database is in memory
            services.AddSingleton<IBasketRepository, BasketRepository>();

            //Services and applications are scoped so we use the same object per request 
            services.AddScoped<IBasketService, BasketService>();
            services.AddScoped<IManageBasketApplication, ManageBasketApplication>();
        }
    }
}
