﻿using Cabify.Basket.Domain.ACLs;
using Cabify.Basket.Domain.Configuration;
using Cabify.Basket.Domain.DTOs;
using Cabify.Basket.Domain.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Cabify.Basket.Test
{
    public class PromotionACLTests : IDisposable
    {
        private readonly Mock<IOptions<ServiceConfiguration>> _options;

        public PromotionACLTests()
        {
            _options = new Mock<IOptions<ServiceConfiguration>>();
        }

        public void Dispose()
        {
            _options.VerifyAll();
        }

        [Fact]
        public async Task ApplyPromotion_Success_ShouldReturnProduct()
        {
            var httpClient = MockHttpClient(HttpStatusCode.OK, new ApplyPromotionResponse() { PromotionProducts = new List<Product>() { new Product("P1", "Test", 10) } });
            _options.Setup(x => x.Value).Returns(new ServiceConfiguration() { PromotionServiceConfig = new PromotionServiceConfig() { BaseUrl = "http://test.com/" } });
            var acl = new PromotionACL(httpClient, _options.Object, Mock.Of<ILogger<PromotionACL>>());

            var promotions = await acl.ApplyPromotions(new List<Product>() { new Product("P1","Test",10) });
            Assert.NotNull(promotions);
            Assert.Single(promotions);
            Assert.Equal("P1", promotions.First().Code);
        }

        [Fact]
        public async Task GetProduct_NotSuccess_ShouldThrowException()
        {
            var httpClient = MockHttpClient(HttpStatusCode.InternalServerError, new ApplyPromotionResponse() { PromotionProducts = new List<Product>() { new Product("P1", "Test", 10) } });
            _options.Setup(x => x.Value).Returns(new ServiceConfiguration() { PromotionServiceConfig = new PromotionServiceConfig() { BaseUrl = "http://test.com/" } });
            var acl = new PromotionACL(httpClient, _options.Object, Mock.Of<ILogger<PromotionACL>>());

            await Assert.ThrowsAsync<HttpRequestException>(() => acl.ApplyPromotions(new List<Product>() { new Product("P1", "Test", 10) }));
        }


        private HttpClient MockHttpClient(HttpStatusCode httpStatusCode, object content)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = httpStatusCode,
                   Content = new StringContent(JsonConvert.SerializeObject(content)),
               })
               .Verifiable();

            return new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };
        }
    }
}
