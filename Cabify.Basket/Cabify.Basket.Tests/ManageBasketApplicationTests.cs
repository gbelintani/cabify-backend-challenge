﻿using Cabify.Basket.Application;
using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Cabify.Basket.Test
{
    public class ManageBasketApplicationTests : IDisposable
    {
        private readonly Mock<IProductACL> _productACL;
        private readonly Mock<IBasketService> _basketService;
        private readonly Mock<IPromotionACL> _promotionACL;

        public ManageBasketApplicationTests()
        {
            _productACL = new Mock<IProductACL>();
            _basketService = new Mock<IBasketService>();
            _promotionACL = new Mock<IPromotionACL>();
        }

        public void Dispose()
        {
            _productACL.VerifyAll();
            _basketService.VerifyAll();
            _promotionACL.VerifyAll();
        }

        [Fact]
        public async Task AddProduct_ValidBasketAndProduct_ReturnsTrue()
        {
            var basketId = Guid.NewGuid();
            _productACL.Setup(x => x.GetProduct(It.Is<string>(s => s.Equals("P1")))).ReturnsAsync(new Product("P1", "Test", 10));
            _basketService.Setup(x => x.AddProduct(It.Is<Guid>(g => g.Equals(basketId)), It.Is<Product>(p => p.Code.Equals("P1")))).Returns(true);

            var application = new ManageBasketApplication(_basketService.Object, _productACL.Object, _promotionACL.Object);
            var result = await application.AddProduct(basketId, "P1");

            Assert.True(result);
        }

        [Fact]
        public async Task AddProduct_InvalidBasket_ReturnsFalse()
        {
            var application = new ManageBasketApplication(_basketService.Object, _productACL.Object, _promotionACL.Object);
            var result = await application.AddProduct(Guid.Empty, "P1");

            Assert.False(result);
        }

        [Fact]
        public async Task AddProduct_InvalidProduct_ReturnsFalse()
        {
            var application = new ManageBasketApplication(_basketService.Object, _productACL.Object, _promotionACL.Object);
            var result = await application.AddProduct(Guid.NewGuid(), null);

            Assert.False(result);
        }

        [Fact]
        public async Task AddProduct_ProductNotFoundOnACL_ReturnsFalse()
        {
            _productACL.Setup(x => x.GetProduct(It.Is<string>(s => s.Equals("P1")))).ReturnsAsync((Product)null);
            var application = new ManageBasketApplication(_basketService.Object, _productACL.Object, _promotionACL.Object);
            var result = await application.AddProduct(Guid.NewGuid(), "P1");

            Assert.False(result);
        }

        [Fact]
        public async Task GetTotal_BasketExistsOnePromotionApplied_ShouldReturnBasketWithOneMoreItem()
        {
            var basketId = Guid.NewGuid();
            _promotionACL.Setup(x => x.ApplyPromotions(It.IsAny<IEnumerable<Product>>())).ReturnsAsync(new List<Product>() { new Product("PROMO", "Promotion", -5) });
            _basketService.Setup(x => x.Get(It.Is<Guid>(g => g.Equals(basketId)))).Returns(new Domain.Models.Basket(basketId, new List<Product>() { new Product("P1", "Test", 10) }));
            var application = new ManageBasketApplication(_basketService.Object, _productACL.Object, _promotionACL.Object);
            var result = await application.GetTotal(basketId);

            Assert.Equal(2, result.Products.Count);
            Assert.Equal(5, result.TotalSum);
        }
    }
}
