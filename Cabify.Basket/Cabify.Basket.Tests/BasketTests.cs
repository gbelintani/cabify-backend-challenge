using Cabify.Basket.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Cabify.Basket.Tests
{
    public class BasketTests
    {
        [Fact]
        public void ShouldCreateNewModel()
        {
            var basket = new Domain.Models.Basket();

            Assert.NotNull(basket.Id);
            Assert.NotNull(basket.Products);
            Assert.Empty(basket.Products);
            Assert.Equal(0, basket.TotalSum);
        }

        [Fact]
        public void ShouldSetCorrectValuesOnNewModel()
        {
            var basket = new Domain.Models.Basket(new Guid("022795e6-779a-4078-aae9-3ab832a3f238"), new List<Product>()
            {
                new Product("P1","Test",10)
            });

            Assert.Equal("022795e6-779a-4078-aae9-3ab832a3f238", basket.Id.ToString());
            Assert.NotNull(basket.Products);
            Assert.Single(basket.Products);
            Assert.Equal(10, basket.TotalSum);
        }
    }
}
