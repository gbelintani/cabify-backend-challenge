﻿using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Models;
using Cabify.Basket.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Cabify.Basket.Test
{
    public class BasketServiceTests : IDisposable
    {
        private readonly Mock<IBasketRepository> _basketRepository;

        public BasketServiceTests()
        {
            _basketRepository = new Mock<IBasketRepository>();
        }

        public void Dispose()
        {
            _basketRepository.VerifyAll();
        }

        [Fact]
        public void ShouldGetAllBaskets()
        {
            var basket1 = Guid.NewGuid();
            var basket2 = Guid.NewGuid();
            _basketRepository.Setup(x => x.GetAll()).Returns(new List<Domain.Models.Basket>()
            {
                GenerateDummyBasket(basket1, 0),
                GenerateDummyBasket(basket2, 0)
            });

            var service = new BasketService(_basketRepository.Object);
            var result = service.GetAll();

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
            Assert.Equal(basket1, result.First().Id);
        }

        [Fact]
        public void GetSpecificBasket_Exists_ShouldReturnBasket()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Get(It.Is<Guid>(g => g.Equals(basket1)))).Returns(GenerateDummyBasket(basket1, 1));

            var service = new BasketService(_basketRepository.Object);

            var basket = service.Get(basket1);

            Assert.NotNull(basket);
            Assert.Equal(basket1, basket.Id);
            Assert.Equal(1, basket.Products.Count);
        }

        [Fact]
        public void GetSpecificBasket_DoesntExists_ShouldReturnNull()
        {
            var service = new BasketService(_basketRepository.Object);

            var basket = service.Get(Guid.NewGuid());

            Assert.Null(basket);
        }

        [Fact]
        public void AddProduct_BasketExists_ShouldReturnTrue()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Update(It.IsAny<Domain.Models.Basket>())).Returns(true);
            _basketRepository.Setup(x => x.Get(It.IsAny<Guid>())).Returns(GenerateDummyBasket(basket1, 0));

            var service = new BasketService(_basketRepository.Object);

            var result = service.AddProduct(basket1, new Product("P1","Teste",1));

            Assert.True(result);
        }

        [Fact]
        public void AddProduct_BasketDoesntExists_ShouldReturnFalse()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Get(It.IsAny<Guid>())).Returns((Domain.Models.Basket)null);

            var service = new BasketService(_basketRepository.Object);

            var result = service.AddProduct(basket1, new Product("P1", "Teste", 1));

            Assert.False(result);
        }

        [Fact]
        public void AddProduct_ProductIsNull_ShouldReturnFalse()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Get(It.IsAny<Guid>())).Returns(GenerateDummyBasket(basket1, 0));

            var service = new BasketService(_basketRepository.Object);

            var result = service.AddProduct(basket1, new Product("P1", "Teste", 1));

            Assert.False(result);
        }

        [Fact]
        public void RemoveProduct_BasketExists_ShouldReturnTrue()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Get(It.IsAny<Guid>())).Returns(GenerateDummyBasket(basket1, 1));
            _basketRepository.Setup(x => x.Update(It.IsAny<Domain.Models.Basket>())).Returns(true);

            var service = new BasketService(_basketRepository.Object);

            var result = service.RemoveProduct(basket1, "P1");

            Assert.True(result);
        }

        [Fact]
        public void RemoveProduct_ProductNotInBasket_ShouldReturnTrue()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Get(It.IsAny<Guid>())).Returns(GenerateDummyBasket(basket1, 0));

            var service = new BasketService(_basketRepository.Object);

            var result = service.RemoveProduct(basket1, "PA");

            Assert.True(result);
        }

        [Fact]
        public void RemoveBasket_BasketExists_ShouldReturnTrue()
        {
            var basket1 = Guid.NewGuid();
            _basketRepository.Setup(x => x.Remove(It.Is<Guid>(g=>g.Equals(basket1)))).Returns(true);

            var service = new BasketService(_basketRepository.Object);

            var result = service.Remove(basket1);

            Assert.True(result);
        }

        private Domain.Models.Basket GenerateDummyBasket(Guid guid, int numberOfProducts)
        {
            var products = new List<Product>();
            for (var i = 0; i < numberOfProducts; i++)
            {
                products.Add(new Product($"P{i+1}", $"Product{i+1}", i+1));
            }
            return new Domain.Models.Basket(guid, products);
        }
    }
}
