﻿using Cabify.Basket.Domain.Models;
using Xunit;

namespace Cabify.Basket.Test
{
    public class ProductTests
    {

        [Fact]
        public void ShouldCreateProductWithValues()
        {
            var product = new Product("Code", "Name", 10);
            Assert.Equal("Code", product.Code);
            Assert.Equal("Name", product.Name);
            Assert.Equal(10, product.Price);
        }
    }
}
