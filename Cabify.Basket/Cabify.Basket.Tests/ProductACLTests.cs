﻿using Cabify.Basket.Domain.ACLs;
using Cabify.Basket.Domain.Configuration;
using Cabify.Basket.Domain.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Cabify.Basket.Test
{
    public class ProductACLTests : IDisposable
    {
        private readonly Mock<IOptions<ServiceConfiguration>> _options;

        public ProductACLTests()
        {
            _options = new Mock<IOptions<ServiceConfiguration>>();
        }

        public void Dispose()
        {
            _options.VerifyAll();
        }

        [Fact]
        public async Task GetProduct_Success_ShouldReturnProduct()
        {
            var httpClient = MockHttpClient(HttpStatusCode.OK, new Product("P1", "Test", 10));
            _options.Setup(x => x.Value).Returns(new ServiceConfiguration() { ProductServiceConfig = new ProductServiceConfig() { BaseUrl = "http://test.com/" } });
            var acl = new ProductACL(httpClient, _options.Object, Mock.Of<ILogger<ProductACL>>());

            var product = await acl.GetProduct("P1");
            Assert.NotNull(product);
            Assert.Equal("P1", product.Code);
        }

        [Fact]
        public async Task GetProduct_NotSuccess_ShouldThrowException()
        {
            var httpClient = MockHttpClient(HttpStatusCode.InternalServerError, new Product("P1", "Test", 10));
            _options.Setup(x => x.Value).Returns(new ServiceConfiguration() { ProductServiceConfig = new ProductServiceConfig() { BaseUrl = "http://test.com/" } });
            var acl = new ProductACL(httpClient, _options.Object, Mock.Of<ILogger<ProductACL>>());

            await Assert.ThrowsAsync<HttpRequestException>(() => acl.GetProduct("P1"));
        }

        [Fact]
        public async Task GetProduct_ProductNotFound_ShouldThrowException()
        {
            var httpClient = MockHttpClient(HttpStatusCode.NotFound, new Product("P1", "Test", 10));
            _options.Setup(x => x.Value).Returns(new ServiceConfiguration() { ProductServiceConfig = new ProductServiceConfig() { BaseUrl = "http://test.com/" } });
            var acl = new ProductACL(httpClient, _options.Object, Mock.Of<ILogger<ProductACL>>());
            var result = await acl.GetProduct("P1");
            Assert.Null(result);
        }

        private HttpClient MockHttpClient(HttpStatusCode httpStatusCode, object content)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = httpStatusCode,
                   Content = new StringContent(JsonConvert.SerializeObject(content)),
               })
               .Verifiable();

            return new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("http://test.com/"),
            };
        }
    }
}
