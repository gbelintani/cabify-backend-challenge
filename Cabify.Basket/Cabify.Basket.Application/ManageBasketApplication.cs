﻿using Cabify.Basket.Domain.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cabify.Basket.Application
{
    /// <summary>
    /// This project has a Application layer, since we have to orchestrate different services to do the operations regarding
    /// all the domains we have. We have the BasketService on this domain and two ACLs regarding the Product and Promotion domains
    /// </summary>
    public class ManageBasketApplication : IManageBasketApplication
    {
        private readonly IBasketService _basketService;
        private readonly IProductACL _productACL;
        private readonly IPromotionACL _promotionACL;

        public ManageBasketApplication(IBasketService basketService, IProductACL productACL, IPromotionACL promotionACL)
        {
            _basketService = basketService;
            _productACL = productACL;
            _promotionACL = promotionACL;
        }

        public async Task<Domain.Models.Basket> GetTotal(Guid basketId)
        {
            if (basketId == null || basketId.Equals(Guid.Empty)) { return null; }
            var basket = _basketService.Get(basketId);
            if(basket == null)
            {
                return null;
            }
            var totalProducts = basket.Products;
            if(basket.Products.Count > 0)
            {
                var promotionItems = await _promotionACL.ApplyPromotions(basket.Products);
                totalProducts = totalProducts.Concat(promotionItems).ToList();
            }
            return new Domain.Models.Basket(basket.Id, totalProducts);
        }

        public async Task<bool> AddProduct(Guid basketId, string productCode)
        {
            if(basketId == null || basketId.Equals(Guid.Empty) || string.IsNullOrEmpty(productCode)) { return false; }
            var product = await _productACL.GetProduct(productCode);
            if(product == null)
            {
                return false;
            }
            return _basketService.AddProduct(basketId, product);
        }

        public bool RemoveProduct(Guid basketId, string productCode)
        {
            if (basketId == null || basketId.Equals(Guid.Empty) || string.IsNullOrEmpty(productCode)) { return false; }
            
            return _basketService.RemoveProduct(basketId, productCode);
        }
    }
}
