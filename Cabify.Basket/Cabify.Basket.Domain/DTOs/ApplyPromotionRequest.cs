﻿using Cabify.Basket.Domain.Models;
using System.Collections.Generic;

namespace Cabify.Basket.Domain.DTOs
{
    /// <summary>
    /// Since we have a request object in the promotion microservice we have this DTO
    /// </summary>
    public class ApplyPromotionRequest
    {
        public IEnumerable<Product> ProductsInBasket { get; set; }
    }
}
