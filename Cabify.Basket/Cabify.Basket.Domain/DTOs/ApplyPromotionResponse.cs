﻿using Cabify.Basket.Domain.Models;
using System.Collections.Generic;

namespace Cabify.Basket.Domain.DTOs
{
    /// <summary>
    /// Since we have a response object in the promotion microservice we have this DTO
    /// </summary>
    public class ApplyPromotionResponse
    {
        public IEnumerable<Product> PromotionProducts { get; set; }
    }
}
