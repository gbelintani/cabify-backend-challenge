﻿using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cabify.Basket.Domain.Services
{
    public class BasketService : IBasketService
    {
        private readonly IBasketRepository _basketRepository;

        /// <summary>
        /// This service will handle the details regarding the basket operations
        /// </summary>
        /// <param name="basketRepository"></param>
        public BasketService(IBasketRepository basketRepository)
        {
            _basketRepository = basketRepository;
        }

        public IEnumerable<Models.Basket> GetAll()
        {
            return _basketRepository.GetAll();
        }

        public Models.Basket Get(Guid guid)
        {
            return _basketRepository.Get(guid);
        }

        public Guid New()
        {
            var basket = new Models.Basket();
            if (_basketRepository.Create(basket))
            {
                return basket.Id;
            }
            return Guid.Empty;
        }

        public bool AddProduct(Guid basketId, Product product)
        {
            var basket = _basketRepository.Get(basketId);
            if (product == null || basket == null)
            {
                return false;
            }
            basket.Products.Add(product);
            return _basketRepository.Update(basket);
        }

        public bool RemoveProduct(Guid basketId, string productCode)
        {
            var basket = _basketRepository.Get(basketId);
            var productToBeRemoved = basket.Products.FirstOrDefault(x => x.Code.Equals(productCode));
            if (productToBeRemoved == null)
            {
                return true;
            }
            basket.Products.Remove(productToBeRemoved);
            return _basketRepository.Update(basket);
        }

        public bool Remove(Guid basketId)
        {
            return _basketRepository.Remove(basketId);
        }
    }
}
