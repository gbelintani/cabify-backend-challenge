﻿using System;
using System.Collections.Generic;

namespace Cabify.Basket.Domain.Interfaces
{
    public interface IBasketRepository
    {
        IEnumerable<Domain.Models.Basket> GetAll();
        Domain.Models.Basket Get(Guid guid);
        bool Create(Domain.Models.Basket basket);
        bool Update(Domain.Models.Basket basket);
        bool Remove(Guid basketId);
    }
}
