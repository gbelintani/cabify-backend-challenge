﻿using System;
using System.Threading.Tasks;

namespace Cabify.Basket.Domain.Interfaces
{
    public interface IManageBasketApplication
    {
        Task<bool> AddProduct(Guid basketId, string productCode);
        bool RemoveProduct(Guid basketId, string productCode);
        Task<Domain.Models.Basket> GetTotal(Guid basketId);
    }
}
