﻿using Cabify.Basket.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cabify.Basket.Domain.Interfaces
{
    public interface IPromotionACL
    {
        Task<IEnumerable<Product>> ApplyPromotions(IEnumerable<Product> basketProducts);
    }
}
