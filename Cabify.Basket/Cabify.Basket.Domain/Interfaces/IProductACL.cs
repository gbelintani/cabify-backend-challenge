﻿using Cabify.Basket.Domain.Models;
using System.Threading.Tasks;

namespace Cabify.Basket.Domain.Interfaces
{
    public interface IProductACL
    {
        Task<Product> GetProduct(string code);
    }
}
