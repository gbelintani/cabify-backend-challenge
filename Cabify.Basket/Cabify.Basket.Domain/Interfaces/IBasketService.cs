﻿using Cabify.Basket.Domain.Models;
using System;
using System.Collections.Generic;

namespace Cabify.Basket.Domain.Interfaces
{
    public interface IBasketService
    {
        IEnumerable<Models.Basket> GetAll();
        Models.Basket Get(Guid guid);
        Guid New();
        bool AddProduct(Guid basketId, Product product);
        bool RemoveProduct(Guid basketId, string productCode);
        bool Remove(Guid basketId);
    }
}
