﻿using Cabify.Basket.Domain.Configuration;
using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Models;
using Flurl;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cabify.Basket.Domain.ACLs
{
    /// <summary>
    /// This ACL uses HTTP to make the call to the Product microservice, since it has a proper interface, we could implement any other
    /// kind of communication layer between the services and not break the rest of the application.
    /// </summary>
    public class ProductACL : IProductACL
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<ServiceConfiguration> _options;
        private readonly ILogger<ProductACL> _logger;

        public ProductACL(HttpClient httpClient, IOptions<ServiceConfiguration> options, ILogger<ProductACL> logger)
        {
            _httpClient = httpClient;
            _options = options;
            _logger = logger;
        }
        public async Task<Product> GetProduct(string code)
        {
            var url = _options.Value.ProductServiceConfig.BaseUrl.AppendPathSegment(code);
            _logger.LogInformation($"URL: {url}");
            var result = await _httpClient.GetAsync(url);
            if(result.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
            result.EnsureSuccessStatusCode();
            var content = await result.Content.ReadAsStringAsync();
            _logger.LogInformation($"Content: {content}");
            return JsonConvert.DeserializeObject<Product>(content);
        }
    }
}
