﻿using Cabify.Basket.Domain.Configuration;
using Cabify.Basket.Domain.DTOs;
using Cabify.Basket.Domain.Interfaces;
using Cabify.Basket.Domain.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cabify.Basket.Domain.ACLs
{
    /// <summary>
    /// This ACL uses HTTP to make the call to the Promotion microservice, since it has a proper interface, we could implement any other
    /// kind of communication layer between the services and not break the rest of the application.
    /// </summary>
    public class PromotionACL : IPromotionACL
    {
        private readonly HttpClient _httpClient;
        private readonly IOptions<ServiceConfiguration> _options;
        private readonly ILogger<PromotionACL> _logger;

        public PromotionACL(HttpClient httpClient, IOptions<ServiceConfiguration> options, ILogger<PromotionACL> logger)
        {
            _httpClient = httpClient;
            _options = options;
            _logger = logger;
        }

        public async Task<IEnumerable<Product>> ApplyPromotions(IEnumerable<Product> basketProducts)
        {
            var url = _options.Value.PromotionServiceConfig.BaseUrl;
            _logger.LogInformation($"URL: {url}");
            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };
            var jsonContent = JsonConvert.SerializeObject(new ApplyPromotionRequest() { ProductsInBasket = basketProducts }, new JsonSerializerSettings()
            {
                ContractResolver = contractResolver
            });
            var result = await _httpClient.PostAsync(url, new StringContent(jsonContent, Encoding.UTF8, "application/json"));
            result.EnsureSuccessStatusCode();
            var content = await result.Content.ReadAsStringAsync();
            _logger.LogInformation($"Content: {content}");
            var response = JsonConvert.DeserializeObject<ApplyPromotionResponse>(content);
            return response.PromotionProducts;
        }
    }
}
