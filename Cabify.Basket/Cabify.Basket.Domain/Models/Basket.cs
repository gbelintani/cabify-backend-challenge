﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cabify.Basket.Domain.Models
{
    public class Basket
    {
        public Guid Id { get; private set; }
        public IList<Product> Products { get; private set; }
        public decimal TotalSum => Products.Sum(x => x.Price);

        public Basket()
        {
            Products = new List<Product>();
            Id = Guid.NewGuid();
        }

        public Basket(Guid id, IList<Product> products)
        {
            Id = id;
            Products = products;
        }

    }
}
