﻿namespace Cabify.Basket.Domain.Models
{
    public class Product
    {
        public string Code { get; private set; }
        public string Name { get; private set; }
        public decimal Price { get; private set; }

        public Product(string code, string name, decimal price)
        {
            Code = code;
            Name = name;
            Price = price;
        }
    }
}
