﻿namespace Cabify.Basket.Domain.Configuration
{
    /// <summary>
    /// This is a Configuration auxiliary class to get the endpoints we need
    /// </summary>
    public class ServiceConfiguration
    {
        public ProductServiceConfig ProductServiceConfig { get; set; }
        public PromotionServiceConfig PromotionServiceConfig { get; set; }
    }

    public class ProductServiceConfig
    {
        public string BaseUrl { get; set; }
    }

    public class PromotionServiceConfig
    {
        public string BaseUrl { get; set; }
    }
}
