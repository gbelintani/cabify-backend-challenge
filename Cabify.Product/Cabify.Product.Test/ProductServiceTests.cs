﻿using Cabify.Product.Domain.Interfaces;
using Cabify.Product.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Cabify.Product.Test
{
    public class ProductServiceTests : IDisposable
    {
        private readonly Mock<IProductRepository> _productRepository;

        public ProductServiceTests()
        {
            _productRepository = new Mock<IProductRepository>();
        }

        public void Dispose()
        {
            _productRepository.VerifyAll();
        }

        [Fact]
        public void ShouldGetAllProducts()
        {
            _productRepository.Setup(x => x.GetAll()).Returns(new List<Domain.Models.Product>()
            {
                GenerateDummyProduct(1),
                GenerateDummyProduct(2)
            });
            var service = new ProductService(_productRepository.Object);

            var products = service.GetAll();

            Assert.NotNull(products);
            Assert.Equal(2, products.Count());
        }

        [Fact]
        public void GetSpecificProduct_Exists_ShouldReturnP1()
        {
            _productRepository.Setup(x => x.Get(It.Is<string>(i => i.Equals("P1")))).Returns(GenerateDummyProduct(1));
            var service = new ProductService(_productRepository.Object);

            var product = service.Get("P1");

            Assert.NotNull(product);
            Assert.Equal("P1", product.Code);
            Assert.Equal("Product1", product.Name);
            Assert.Equal(1M, product.Price);
        }

        [Fact]
        public void GetSpecificProduct_ExistsCaseInsensitive_ShouldReturnP2()
        {
            _productRepository.Setup(x => x.Get(It.Is<string>(i => i.Equals("P2")))).Returns(GenerateDummyProduct(2));

            var service = new ProductService(_productRepository.Object);

            var product = service.Get("p2");

            Assert.NotNull(product);
            Assert.Equal("P2", product.Code);
            Assert.Equal("Product2", product.Name);
            Assert.Equal(2M, product.Price);
        }

        [Fact]
        public void GetSpecificProduct_DoesntExists_ShouldReturnNull()
        {
            var service = new ProductService(_productRepository.Object);

            var product = service.Get("P2");

            Assert.Null(product);
        }

        private Domain.Models.Product GenerateDummyProduct(int number)
        {
            return new Domain.Models.Product($"P{number}", $"Product{number}", number);
        }
    }
}
