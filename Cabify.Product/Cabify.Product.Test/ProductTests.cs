﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Cabify.Product.Test
{

    public class ProductTests
    {

        [Fact]
        public void ShouldCreateProductWithValues()
        {
            var product = new Domain.Models.Product("Code","Name",10);
            Assert.Equal("Code", product.Code);
            Assert.Equal("Name", product.Name);
            Assert.Equal(10, product.Price);
        }
    }
}
