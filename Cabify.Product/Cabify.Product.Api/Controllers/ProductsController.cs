﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cabify.Product.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Cabify.Product.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet("{code}")]
        public ActionResult<Domain.Models.Product> Get(string code)
        {
            try
            {
                var result = _productService.Get(code);
                if(result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return NotFound();
                }
            }catch(Exception)
            {
                //Log
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet()]
        public ActionResult<IEnumerable<Domain.Models.Product>> Get()
        {
            return Ok(_productService.GetAll());
        }

      
    }
}
