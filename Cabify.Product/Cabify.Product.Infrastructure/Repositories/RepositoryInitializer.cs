﻿using Cabify.Product.Domain.Interfaces;

namespace Cabify.Product.Infrastructure.Repositories
{
    public class RepositoryInitializer
    {
        private readonly IProductRepository _productRepository;

        /// <summary>
        /// This is just an auxiliary class to seed the proposed products
        /// </summary>
        /// <param name="productRepository"></param>
        public RepositoryInitializer(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void Seed()
        {
            _productRepository.Create(new Domain.Models.Product(
                "VOUCHER",
                "Cabify Voucher",
                5
            ));
            _productRepository.Create(new Domain.Models.Product(
                "TSHIRT",
                "Cabify T-Shirt",
                20
            ));
            _productRepository.Create(new Domain.Models.Product(
                "MUG",
                "Cabify Coffee Mug",
                7.5M
            ));
        }
    }
}
