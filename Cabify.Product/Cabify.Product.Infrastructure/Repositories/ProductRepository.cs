﻿using Cabify.Product.Domain.Interfaces;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Cabify.Product.Infrastructure.Repositories
{
    /// <summary>
    /// If we had more repositories to worry about, this could easily be transformed in a generic abstract class with <TKey,TData></TKey>
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private readonly ConcurrentDictionary<string, Domain.Models.Product> _keyValues;

        public ProductRepository()
        {
            _keyValues = new ConcurrentDictionary<string, Domain.Models.Product>();
        }

        public bool Create(Domain.Models.Product product)
        {
            return _keyValues.TryAdd(product.Code, product);
        }

        public Domain.Models.Product Get(string code)
        {
            _keyValues.TryGetValue(code, out var value);
            return value;
        }

        public IEnumerable<Domain.Models.Product> GetAll()
        {
            return _keyValues.Values;
        }
    }
}
