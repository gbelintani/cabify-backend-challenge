﻿using Cabify.Product.Domain.Interfaces;
using System.Collections.Generic;

namespace Cabify.Product.Domain.Services
{
    /// <summary>
    /// This service may seem unecessary because it does basically the same as the repository in this case, 
    /// but if any new business logic comes up it should go here 
    /// and let the controller and repository do their job(which should not be business logic :P)
    /// </summary>
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }


        public IEnumerable<Models.Product> GetAll()
        {
            return _productRepository.GetAll();
        }

        public Models.Product Get(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }
            return _productRepository.Get(code.ToUpper());
        }
    }
}
