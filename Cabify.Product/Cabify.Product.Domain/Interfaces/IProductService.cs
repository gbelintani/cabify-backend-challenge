﻿using System.Collections.Generic;

namespace Cabify.Product.Domain.Interfaces
{
    public interface IProductService
    {
        IEnumerable<Models.Product> GetAll();
        Models.Product Get(string code);
    }
}
