﻿using System;
using System.Collections.Generic;
using System.Text;
using Cabify.Product.Domain.Models;

namespace Cabify.Product.Domain.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Models.Product> GetAll();

        Models.Product Get(string code);
        bool Create(Models.Product product);
    }
}
