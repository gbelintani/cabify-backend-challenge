﻿using Cabify.Product.Domain.Interfaces;
using Cabify.Product.Domain.Services;
using Cabify.Product.Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Cabify.Product.IoC
{
    public class DependencyInjection
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //Repositories are singleton since the database is in memory
            services.AddSingleton<IProductRepository, ProductRepository>();

            //Services and applications are scoped so we use the same object per request 
            services.AddScoped<IProductService, ProductService>();
        }
    }
}
